package br.com.cielo.dojo.tdd.domains;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class Product {
	private Long id;
	private String sku;
	private String name;
	private BigDecimal price;
	private BigDecimal freightRate;
}
