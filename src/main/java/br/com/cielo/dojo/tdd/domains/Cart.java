package br.com.cielo.dojo.tdd.domains;

import java.time.LocalDateTime;
import java.util.List;

import lombok.Data;

@Data
public class Cart {
	private Long id;
	private LocalDateTime createAt;
	private String cardNumber;
	private List<Product> products;
}
