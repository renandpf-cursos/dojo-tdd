package br.com.cielo.dojo.tdd.usecase;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.cielo.dojo.tdd.CardFieldsValidator;
import br.com.cielo.dojo.tdd.domains.Cart;
import br.com.cielo.dojo.tdd.domains.Order;
import br.com.cielo.dojo.tdd.usecase.exception.InvalidCardNumberException;

public class PlaceOrder {

	@Autowired
	private CardFieldsValidator cardFieldsValidator;
	
	@Autowired
	private DecryptAndValidCardNumber decryptAndValidCardNumber;
	
	public Order place(final Cart cart) throws InvalidCardNumberException {
		
		//TODO converter cart para order
		Order order = new Order();
		order.setCardNumber("any");
		
		final boolean cardDecryptValid = cardValidate(order);
		
		verifyCardNumber(cardDecryptValid);
		
		return null;
	}

	private boolean cardValidate(Order order) {
		final boolean cardValid = this.cardFieldsValidator.validate(order.getCardNumber());
		final boolean cardDecryptValid = this.decryptAndValidCardNumber.decryptAndValidCardNumber(order.getCardNumber());
		
		return cardDecryptValid && cardValid ;
	}

	private void verifyCardNumber(final boolean cardDecryptValid) throws InvalidCardNumberException {
		if(!cardDecryptValid) {
			throw new InvalidCardNumberException();
		}
	}

}
