package br.com.cielo.dojo.tdd.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Scope("request")
@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value="orders")
public class OrderController {
	


}
