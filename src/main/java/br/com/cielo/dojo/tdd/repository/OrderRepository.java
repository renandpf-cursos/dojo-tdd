package br.com.cielo.dojo.tdd.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.cielo.dojo.tdd.domains.Order;

public interface OrderRepository extends MongoRepository<Order, Long>{

}
