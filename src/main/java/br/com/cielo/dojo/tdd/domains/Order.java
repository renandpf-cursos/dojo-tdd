package br.com.cielo.dojo.tdd.domains;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import lombok.Data;

@Data
public class Order {
	private Long id;
	private LocalDateTime createdAt;
	private Customer customer;
	private List<Product> products;
	private BigDecimal totalCostProducts;
	private BigDecimal totalCostFreight;
	private String cardNumber;
	
}
