package br.com.cielo.dojo.tdd.usecase;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;

import br.com.cielo.dojo.tdd.CardFieldsValidator;
import br.com.cielo.dojo.tdd.domains.Cart;
import br.com.cielo.dojo.tdd.domains.Order;
import br.com.cielo.dojo.tdd.usecase.exception.InvalidCardNumberException;

public class PlaceOrderUnitTest {

	@InjectMocks
	private PlaceOrder placeOrder = new PlaceOrder();
	
	@Mock
	private CardFieldsValidator cardFieldsValidator;
	
	@Mock
	private DecryptAndValidCardNumber decryptAndValidCardNumber;
	
    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }
	
    
	@Test
	public void placeSuccess() throws InvalidCardNumberException {
		final Order order = new Order();
		order.setCardNumber("any");
		
		Mockito.when(cardFieldsValidator.validate(anyString())).thenReturn(true);
		Mockito.when(decryptAndValidCardNumber.decryptAndValidCardNumber(anyString())).thenReturn(true);
		
		placeOrder.place(new Cart());
		
		verify(this.cardFieldsValidator, VerificationModeFactory.times(1)).validate(anyString());
		verify(this.decryptAndValidCardNumber, VerificationModeFactory.times(1)).decryptAndValidCardNumber(anyString());
	}
	
	@Test(expected = InvalidCardNumberException.class)
	public void placeValidateCardError() throws InvalidCardNumberException {
		final Order order = new Order();
		order.setCardNumber("any");

		Mockito.when(cardFieldsValidator.validate(anyString())).thenReturn(false);
		Mockito.when(decryptAndValidCardNumber.decryptAndValidCardNumber(anyString())).thenReturn(false);
		
		placeOrder.place(new Cart());
		
		verify(this.cardFieldsValidator, VerificationModeFactory.times(1)).validate(anyString());
		verify(this.decryptAndValidCardNumber, VerificationModeFactory.times(1)).decryptAndValidCardNumber(anyString());
	}
	
}
